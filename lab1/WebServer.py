#import socket module
from socket import *                                  
serverSocket = socket(AF_INET, SOCK_STREAM)
#Prepare a sever socket
#Fill in start
serverPort = 80
serverAddr = '127.0.0.1'

serverSocket.bind((serverAddr,serverPort))
serverSocket.listen(7)
#Fill in end
while True:
    #Establish the connection
    print ('Ready to serve...')
    connectionSocket, addr = serverSocket.accept()
    try:
        message =  connectionSocket.recv(1024)            
        filename = message.split()[1]                 
        f = open(filename[1:])                        
        outputdata = f.read()
        #Send one HTTP header line into socket
        #Fill in start
        connectionSocket.send('\nHTTP/1.1 200 OK\r\n'.encode())
        #Fill in end                
        #Send the content of the requested file to the client
        for i in range(0, len(outputdata)):           
             connectionSocket.send(outputdata[i].encode())
        connectionSocket.close()
    except IOError:
        #Send response message for file not found
        #Fill in start
        errorMessage = 'HTTP/1.1 404 Not Found\r\n' + 'Content-Type: text/html\r\n\r\n' +  '<html><head></head><body><h1>404 Not Found</h1></body></html>'
        connectionSocket.send(errorMessage.encode())        
        #Fill in end
        #Close client socket
        #Fill in start
        connectionSocket.close()
        #Fill in end
serverSocket.close()        

        
 