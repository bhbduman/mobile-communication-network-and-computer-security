from base64 import b64decode, b64encode
from socket import *
import ssl
sender = "tempirir@gmail.com"
senderpass = "hardpasswd"
receiver = "baranhbozduman@gmail.com"


msg = "\r\n I love computer networks!"
subj = "Subject is not indicated"
endmsg = "\r\n.\r\n"


def print_recv_info(recv):
    recv = recv.decode()
    print(recv)
    codes = ['220', '221', '235', '250', '334', '354' ]
    if not(recv[:3] in codes):
    	print(recv[:3]+ ' reply not received from server')
    

# Choose a mail server (e.g. Google mail server) and call it mailserver
mailserver = ("smtp.gmail.com", 587)
# Create socket called clientSocket and establish a TCP connection with mailserver
#Fill in start  
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect(mailserver)
#Fill in end
recv = clientSocket.recv(1024)
print_recv_info(recv)

# Send HELO command and print server response.
heloCommand = 'HELO Alice\r\n'
clientSocket.send(heloCommand.encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)

    
#To establish tls connection
clientSocket.send("STARTTLS\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
 
clientSocket = ssl.wrap_socket(clientSocket)

clientSocket.send("AUTH LOGIN ".encode() + b64encode(sender.encode()) + "\r\n".encode() )
recv = clientSocket.recv(1024)
print_recv_info(recv)

clientSocket.send(b64encode(senderpass.encode()) + "\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
     

# Send MAIL FROM command and print server response.
# Fill in start
clientSocket.send(f"MAIL FROM: <{sender}>\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
# Fill in end

# Send RCPT TO command and print server response. 
# Fill in start
clientSocket.send(f"RCPT TO: <{receiver}>\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)

# Fill in end
# Send DATA command and print server response. 
# Fill in start
clientSocket.send("Data\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
# Fill in end


# Send message data.
# Message ends with a single period.
# Fill in start
clientSocket.send(f"SUBJECT: {subj}\n{msg + endmsg}".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
# Fill in end

# Send QUIT command and get server response.
# Fill in start
clientSocket.send("Quit\r\n".encode())
recv= clientSocket.recv(1024)
print_recv_info(recv)


clientSocket.close()
# Fill in end
