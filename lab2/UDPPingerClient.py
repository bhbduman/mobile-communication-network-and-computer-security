from socket import *
from time import ctime, time

serverAddr = '127.0.0.1'
serverPort = 12000
connectionSocket = socket(AF_INET, SOCK_DGRAM)
connectionSocket.settimeout(1)

for sequence_number in range(1,11):
    timer = time()
    message = "Ping " + str(sequence_number) + " " + ctime(timer)[11:19]
    connectionSocket.sendto(message.encode(), ('127.0.0.1', 12000))
    
    try:    
        encodedModified, serverAddr = connectionSocket.recvfrom(1024)
        endtimer = time()
        modifiedMessage = encodedModified.decode()
        
        print(modifiedMessage)
        print("Round Trip Time: %.3f ms\n" % ((endtimer - timer)*1000))
        
    except:
        print( str(sequence_number) + " PING Request timed out\n")
        
connectionSocket.close()
    
    
    
