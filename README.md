# Project Report

## LAB1 - Web Server

### Server

1. Firstly determine a server address and port and bind it to a socket
2. Send HTTP header into socket in case of this I send 200 OK message
3. In case of the requested page doesn’t exist send 404 Not Found message
4. Close the socket

```python
#import socket module
from socket import *                                  
serverSocket = socket(AF_INET, SOCK_STREAM)
#Prepare a sever socket
#1 Fill in start
serverPort = 80
serverAddr = '127.0.0.1'
serverSocket.bind((serverAddr,serverPort))
serverSocket.listen(7)
#Fill in end
while True:
    #Establish the connection
    print ('Ready to serve...')
    connectionSocket, addr = serverSocket.accept()
    try:
        message =  connectionSocket.recv(1024)            
        filename = message.split()[1]                 
        f = open(filename[1:])                        
        outputdata = f.read()
        #Send one HTTP header line into socket
        #2 Fill in start
        connectionSocket.send('\nHTTP/1.1 200 OK\r\n'.encode())
        #Fill in end                
        #Send the content of the requested file to the client
        for i in range(0, len(outputdata)):           
             connectionSocket.send(outputdata[i])
        connectionSocket.close()
    except IOError:
        #Send response message for file not found
        #3 Fill in start
        errorMessage = 'HTTP/1.1 404 Not Found\r\n' + 'Content-Type: text/html\r\n\r\n' +  '<html><head></head><body><h1>404 Not Found</h1></body></html>'
        connectionSocket.send(errorMessage.encode())        
        #Fill in end
        #Close client socket
        #4 Fill in start
        connectionSocket.close()
        #Fill in end
serverSocket.close()
```

### Client

1. Determine ip address and port number connect it
2. Get the response messages
3. Close the socket

```python
# Import socket module
import socket            
# Create a socket object
s = socket.socket()        
# Define the port on which you want to connect
port = 80            
# connect to the server on local computer
s.connect(('127.0.0.1', port))
# receive data from the server and decoding to get the string.
print (s.recv(1024).decode())
# close the connection
s.close()
```

### Tests

![Screen Shot 2021-12-28 at 12.51.50.png](doc/Screen_Shot_2021-12-28_at_12.51.50.png)

![Screen Shot 2021-12-28 at 12.49.03.png](doc/Screen_Shot_2021-12-28_at_12.49.03.png)

![Screen Shot 2021-12-28 at 12.51.38.png](doc/Screen_Shot_2021-12-28_at_12.51.38.png)

## LAB2 - UDP Pinger

### Server

1. First we generate a socket with udp configuration
2. Bind it to a adddress and port
3. Receive message from client
4. If random number less than 4 return nothing otherwise send message in uppercase

```python
# UDPPingerServer.py
# We will need the following module to generate randomized lost packetsimport random
from socket import *
import random
# Create a UDP socket 
# Notice the use of SOCK_DGRAM for UDP packets
serverSocket = socket(AF_INET, SOCK_DGRAM)# Assign IP address and port number to socket
serverSocket.bind(('', 12000))

while True:
    # Generate random number in the range of 0 to 10
    rand = random.randint(0, 10)    
    # Receive the client packet along with the address it is coming from 
    message, address = serverSocket.recvfrom(1024)
    # Capitalize the message from the client
    message = message.upper()
    # If rand is less is than 4, we consider the packet lost and do not respond
    if rand < 4:
        continue
    # Otherwise, the server responds    
    serverSocket.sendto(message, address)
```

### Client

1. Set timeout in case of there is no response
2. send ping message sequence number and time
3. Get response and print response time
4. In case of there is no response, print timeout message

```python
from socket import *
from time import ctime, time

serverAddr = '127.0.0.1'
serverPort = 12000
connectionSocket = socket(AF_INET, SOCK_DGRAM)
connectionSocket.settimeout(1)

for sequence_number in range(1,11):
    timer = time()
    message = "Ping " + str(sequence_number) + " " + ctime(timer)[11:19]
    connectionSocket.sendto(message.encode(), ('127.0.0.1', 12000))
    
    try:    
        encodedModified, serverAddr = connectionSocket.recvfrom(1024)
        endtimer = time()
        modifiedMessage = encodedModified.decode()
        
        print(modifiedMessage)
        print("Round Trip Time: %.3f ms\n" % ((endtimer - timer)*1000))
        
    except:
        print( str(sequence_number) + " PING Request timed out\n")
        
connectionSocket.close()

```

### Tests

![lab2.png](doc/lab2.png)

## LAB3 - Mail Client

### Mail Client

1. Create socket with TCP connection
2. Determine mail server
3. Apply ssl to created socket
4. Log in with mail and password
5. Send mail to determined address
6. Quit and close the connection

```python
from base64 import b64decode, b64encode
from socket import *
import ssl
sender = "tempirir@gmail.com"
senderpass = "hardpasswd"
receiver = "baranhbozduman@gmail.com"

msg = "\r\n I love computer networks!"
subj = "Subject is not indicated"
endmsg = "\r\n.\r\n"

def print_recv_info(recv):
    recv = recv.decode()
    print(recv)
    codes = ['220', '221', '235', '250', '334', '354' ]
    if not(recv[:3] in codes):
    	print(recv[:3]+ ' reply not received from server')
    

# Choose a mail server (e.g. Google mail server) and call it mailserver
mailserver = ("smtp.gmail.com", 587)
# Create socket called clientSocket and establish a TCP connection with mailserver
#Fill in start  
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect(mailserver)
#Fill in end
recv = clientSocket.recv(1024)
print_recv_info(recv)

# Send HELO command and print server response.
heloCommand = 'HELO Alice\r\n'
clientSocket.send(heloCommand.encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)

    
#To establish tls connection
clientSocket.send("STARTTLS\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
 
clientSocket = ssl.wrap_socket(clientSocket)

clientSocket.send("AUTH LOGIN ".encode() + b64encode(sender.encode()) + "\r\n".encode() )
recv = clientSocket.recv(1024)
print_recv_info(recv)

clientSocket.send(b64encode(senderpass.encode()) + "\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)

# Send MAIL FROM command and print server response.
# Fill in start
clientSocket.send(f"MAIL FROM: <{sender}>\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
# Fill in end

# Send RCPT TO command and print server response. 
# Fill in start
clientSocket.send(f"RCPT TO: <{receiver}>\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)

# Fill in end
# Send DATA command and print server response. 
# Fill in start
clientSocket.send("Data\r\n".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
# Fill in end

# Send message data.
# Message ends with a single period.
# Fill in start
clientSocket.send(f"SUBJECT: {subj}\n{msg + endmsg}".encode())
recv = clientSocket.recv(1024)
print_recv_info(recv)
# Fill in end

# Send QUIT command and get server response.
# Fill in start
clientSocket.send("Quit\r\n".encode())
recv= clientSocket.recv(1024)
print_recv_info(recv)

clientSocket.close()
# Fill in end
```

### Tests

![Untitled](doc/Untitled.png)

# Baran Hasan Bozduman - 171044036